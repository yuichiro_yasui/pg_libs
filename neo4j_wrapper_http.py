import requests
import json
from collections import defaultdict
import markdown
import tempfile
import pandas as pd
import networkx as nx
from matplotlib import pyplot, colors
from pyvis import network
from IPython.display import display, HTML

class Neo4jGraph(object):
    def __init__(self, uri, user, password):
        self._uri = uri
        self._user = user
        self._password = password

    def run(self, cypher, result_types=['row', 'graph']):
        payload = { 'statements': [] }
        payload['statements'].append(
            { 'statement': cypher, 'resultDataContents': result_types }
        )
        headers = { 'content-type': 'application/json' }
        r = requests.post(
            self._uri, data=json.dumps(payload),
            headers=headers, auth=(self._user, self._password)
        )
        if r.status_code == requests.codes.ok:
            return r.json()
        else:
            return { 'results': [], 'errors': [] }

    def run_as_pd(self, cypher):
        r = self.run(cypher, result_types=['row'])
        x = r['results'][0]
        df = pd.DataFrame([ d['row'] for d in x['data'] ], columns=x['columns'])
        return df

    def run_as_nx(self, cypher, create_using=None):
        r = self.run(cypher, result_types=['graph'])
        x = r['results'][0]
        return self.construct_graph(x, create_using)

    def run_and_zprint(self, cypher, create_using=None):
        r = self.run(cypher, result_types=['row', 'graph'])
        x = r['results'][0]
        g = self.construct_graph(x, create_using)
        if g.number_of_nodes() > 0 or g.number_of_edges() > 0:
            import json
            print("%network\n" + json.dumps(self.nx_to_zpg_dict(g), ensure_ascii=False))
        else:
            import pandas as pd
            from io import StringIO
            df = pd.DataFrame([ d['row'] for d in x['data'] ], columns=x['columns'])
            buf = StringIO()
            df.to_csv(buf, sep='\t', index=False)
            print('%table\n'+ buf.getvalue())

    def run_as_pyviz(self, cypher, create_using=None):
        G = self.run_as_nx(cypher, create_using=create_using)
        with tempfile.NamedTemporaryFile(prefix='pg_', suffix='.html') as fp:
            pg = self.to_pyvis(G, directed=True, get_vlb=lambda x: x['label'], notebook=True)
            pg.save_graph(fp.name)
            display(HTML(open(fp.name).read()))
        return G

    @staticmethod
    def nx_to_zpg_dict(g, cmap=None):
        zpg = {}
        zpg['nodes'] = list(ai for _, ai in g.nodes(data=True))
        zpg['edges'] = list(aij for _, _, aij in g.edges(data=True))
        zpg['directed'] = True
        lb_set = set(ai['label'] for _, ai in g.nodes(data=True))
        cmap = pyplot.get_cmap('tab10', len(lb_set))
        zpg['labels'] = { lb: colors.rgb2hex( cmap(_i)[:3] ) for _i, lb in enumerate(sorted(lb_set)) }
        zpg['types'] = list(set(aij['label'] for _, _, aij in g.edges(data=True)))
        return zpg

    @staticmethod
    def construct_graph(result_dict, create_using=None):
        def has_labeled_edge(G, vi, vj, lb, _t='label'):
            if not G.has_edge(vi, vj):
                return False
            if not G.is_multigraph():
                return True
            return any(
                G.edges[(vi, vj, k)].get(_t, None) == lb
                for k in range(G.new_edge_key(vi, vj))
            )
        if not create_using:
            G = nx.MultiDiGraph()
        else:
            G = create_using
            G.clear()
        for _x in result_dict['data']:
            for v in _x['graph']['nodes']:
                lb = list(set(v['labels']))
                G.add_node(
                    v['id'], id=v['id'],
                    label=lb[0] if lb else 'None', labels=lb,
                    data=v['properties']
                )
            for e in _x['graph']['relationships']:
                vi, vj, lb = e['startNode'], e['endNode'], e['type']
                if has_labeled_edge(G, vi, vj, lb):
                    continue
                G.add_edge(
                    e['startNode'], e['endNode'],
                    source=e['startNode'], target=e['endNode'],
                    id=e['id'], label=e['type'], data=e['properties']
                )
        return G

    @staticmethod
    def to_pyvis(G, get_vlb=None, get_elb=None, vgrp_lb='label', egrp_lb='label', tolb=lambda x: x, **kwargs):
        def dict2html(d):
            if not d: return ''
            return markdown.Markdown(extensions=['tables', 'meta']).convert(
                pd.DataFrame(d.items(), columns=['key', 'value']).to_markdown()
            )
        vg = network.Network(**kwargs)
        v_grp = defaultdict(int, {
            k: i+1 for i, k in enumerate(set([ tolb(lb) for _, lb in G.nodes(data=vgrp_lb) if lb ]))
        })
        e_grp = defaultdict(int, {
            k: i+1 for i, k in enumerate(set([ tolb(lb) for _, _, lb in G.edges(data=egrp_lb) if lb ]))
        })
        for v, attr in G.nodes(data=True):
            lb = get_vlb(attr) if get_vlb else v
            grp = v_grp[ tolb(attr.get(vgrp_lb, '')) ]
            vg.add_node(v, label=lb, title=dict2html(attr), value=G.degree(v)+1, group=grp)
        for vi, vj, attr in G.edges(data=True):
            lb = get_elb(attr) if get_elb else ''
            grp = e_grp[ tolb(attr.get(egrp_lb, '')) ]
            vg.add_edge(vi, vj, label=lb, title=dict2html(attr), group=grp, arrowStrikethrough=False)
        vg.set_edge_smooth('dynamic')
        return vg