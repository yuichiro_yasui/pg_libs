import json
from io import StringIO
import pandas as pd
import networkx as nx
from matplotlib import pyplot, colors
from neo4j import GraphDatabase

class Neo4jGraph(object):
    def __init__(self, uri, user, password, encrypted=False):
        self._driver = GraphDatabase.driver(
            uri, auth=(user, password), encrypted=encrypted
        )

    def close(self):
        self._driver.close()

    def run(self, cq, **kwargs):
        with self._driver.session() as session:
            res = [ dict(x) for x in session.run(cq, **kwargs) ]
        return res

    def run_as_pd(self, cq, **kwargs):
        df = pd.DataFrame(self.run(cq, **kwargs))
        return df

    def run_as_nx(self, cq, create_using=None, **kwargs):
        return self.__construct_graph(self.run(cq, **kwargs), create_using)

    def run_and_zprint(self, cq, create_using=None, **kwargs):
        res = self.run(cq, **kwargs)
        g = self.__construct_graph(res, create_using)
        if g.number_of_nodes() > 0 or g.number_of_edges() > 0:
            print("%network\n" + json.dumps(self.nx_to_zpg_dict(g), ensure_ascii=False))
        else:
            df = pd.DataFrame(res)
            buf = StringIO()
            df.to_csv(buf, sep='\t', index=False)
            print('%table\n'+ buf.getvalue())

    @staticmethod
    def nx_to_zpg_dict(g, cmap=None):
        zpg = {}
        zpg['nodes'] = list(ai for _, ai in g.nodes(data=True))
        zpg['edges'] = list(aij for _, _, aij in g.edges(data=True))
        zpg['directed'] = True
        lb_set = set(ai['label'] for _, ai in g.nodes(data=True))
        cmap = pyplot.get_cmap('tab10', len(lb_set))
        zpg['labels'] = { lb: colors.rgb2hex( cmap(_i)[:3] ) for _i, lb in enumerate(sorted(lb_set)) }
        zpg['types'] = list(set(aij['label'] for _, _, aij in g.edges(data=True)))
        return zpg

    @staticmethod
    def has_labeled_edge(g, vi, vj, lb, _t='label'):
        if not g.has_edge(vi, vj):
            return False
        else:
            if not g.is_multigraph():
                return True
            else:
                return any(
                    g.edges[(vi, vj, k)].get(_t, None) == lb
                    for k in range(g.new_edge_key(vi, vj))
                )

    def __construct_graph(self, dict_list, create_using=None):
        if create_using is None:
            G = nx.MultiDiGraph()
        else:
            G = create_using
            G.clear()
        for r in dict_list:
            for x in r.values():
                s = str(x)
                if s.startswith('<Path'):
                    for _x in x.relationships:
                        self.__append_edge(G, _x)
                elif s.startswith('<Node'):
                    self.__append_node(G, x)
                elif s.startswith('<Relationship'):
                    self.__append_edge(G, x)
                else:
                    pass
                # print('unknown: "{}"'.format(s))
        return G

    def __append_node(self, g, nd):
        lb = list(set(nd.labels))
        g.add_node(nd.id, id=nd.id, label=lb[0] if lb else 'None', labels=lb, data=dict(nd))

    def __append_edge(self, g, ed):
        self.__append_node(g, ed.start_node)
        self.__append_node(g, ed.end_node)
        if not self.has_labeled_edge(g, ed.start_node.id, ed.end_node.id, ed.type):
            g.add_edge(
                ed.start_node.id, ed.end_node.id,
                source=ed.start_node.id, target=ed.end_node.id,
                id=ed.id, label=ed.type, data=dict(ed)
            )