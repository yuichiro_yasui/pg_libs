# PG_LIBS

PG (Property Graph) libraries

## Neo4j Utilities

This library provides an interface for running Cypher queries to Neo4j and outputting them in Pandas DataFrame and NetworkX MultiDiGraph formats. 

- `neo4j_wrapper_http.py` used Neo4j's HTTP API. **recommended.**
- `neo4j_wrapper_bolt.py` used on Neo4j's Bolt API.

```python
from neo4j_wrapper_http import Neo4jGraph

ENDPOINT = "http://XXXXXXXX:7474/db/data/transaction/commit"
USERNAME = ""
PASSWORD = ""
G = Neo4jGraph(ENDPOINT, USERNAME, PASSWORD)

# run Cypher query and return results as dict
results = G.run(cypher_query)

# return results as Pandas Dataframe
df = G.run_as_pd(cypher_query)
```



## PG file visualizer

Notebook to visualize PG (Property Graph) model format.

- `pg_vis.ipynb`